<?php

/**
 * This file is part of the "rico_events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

defined('TYPO3_MODE') || die();

(function ($extensionKey) {
    // Configure events list plugin.
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        "Riconet.$extensionKey",
        \Riconet\RicoEvents\Constants::PLUGIN_NAME_EVENTS,
        [
            'Events' => 'list, show, iCalendar',
        ],
        [
            'Events' => 'list, iCalendar',
        ]
    );

    // Configure events calendar plugin.
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        "Riconet.$extensionKey",
        \Riconet\RicoEvents\Constants::PLUGIN_NAME_CALENDAR,
        [
            'Calendar' => 'index',
            'Events' => 'show',
        ]
    );

    // Add upgrade wizards.
    $wizards = [
        \Riconet\RicoEvents\Updates\SlugifyUpdateWizard::class
    ];
    foreach ($wizards as $class) {
        $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/install']['update'][$class] = $class;
    }
})(\Riconet\RicoEvents\Constants::EXTENSION_KEY);
