<?php

/**
 * This file is part of the "rico_events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

return (function ($extensionKey, $table) {
    $ll = "LLL:EXT:$extensionKey/Resources/Private/Language/locallang_db.xlf";
    $fields = implode(',', [
        'event',
        'description',
        'start_date_time',
        'end_date_time',
    ]);

    return [
        'ctrl' => [
            'title' => "$ll:$table",
            'label' => 'start_date_time',
            'label_userFunc' => \Riconet\RicoEvents\Utility\TcaEventsUtility::class . '->formatDayLabel',
            'tstamp' => 'tstamp',
            'crdate' => 'crdate',
            'cruser_id' => 'cruser_id',
            'languageField' => 'sys_language_uid',
            'transOrigPointerField' => 'l10n_parent',
            'transOrigDiffSourceField' => 'l10n_diffsource',
            'delete' => 'deleted',
            'enablecolumns' => [
                'disabled' => 'hidden',
                'starttime' => 'starttime',
                'endtime' => 'endtime',
            ],
            'searchFields' => $fields,
            'iconfile' => "EXT:$extensionKey/Resources/Public/Icons/$table.gif",
            'default_sortby' => 'start_date_time',
            'hideTable' => true,
        ],
        'types' => [
            '1' => [
                'showitem' => "--palette--;;paletteCore, --palette--;;paletteLanguage, $fields",
            ],
        ],
        'palettes' => [
            'paletteCore' => [
                'showitem' => 'hidden,',
            ],
            'paletteLanguage' => [
                'showitem' => 'sys_language_uid;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:sys_language_uid_formlabel,l10n_parent, l10n_diffsource,',
            ],
        ],
        'columns' => [
            'sys_language_uid' => [
                'exclude' => true,
                'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
                'config' => [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'special' => 'languages',
                    'foreign_table' => 'sys_language',
                    'foreign_table_where' => 'ORDER BY sys_language.title',
                    'items' => [
                        ['LLL:EXT:core/Resources/Private/Language/locallang_general.php:LGL.allLanguages', -1],
                    ],
                    'default' => 0,
                    'eval' => 'int',
                ],
            ],
            'l10n_parent' => [
                'displayCond' => 'FIELD:sys_language_uid:>:0',
                'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
                'config' => [
                    'type' => 'select',
                    'items' => [
                        ['', 0],
                    ],
                    'foreign_table' => 'tx_ricoevents_domain_model_day',
                    'foreign_table_where' => 'AND tx_ricoevents_domain_model_day.pid=###CURRENT_PID### AND tx_ricoevents_domain_model_day.sys_language_uid IN (-1,0)',
                    'eval' => 'int',
                ],
            ],
            'l10n_diffsource' => [
                'config' => [
                    'type' => 'passthrough',
                ],
            ],
            'hidden' => [
                'exclude' => true,
                'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
                'config' => [
                    'type' => 'check',
                ],
            ],
            'starttime' => [
                'exclude' => true,
                'l10n_mode' => 'mergeIfNotBlank',
                'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
                'config' => [
                    'type' => 'input',
                    'renderType' => 'inputDateTime',
                    'eval' => 'datetime',
                ],
            ],
            'endtime' => [
                'exclude' => true,
                'l10n_mode' => 'mergeIfNotBlank',
                'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
                'config' => [
                    'type' => 'input',
                    'renderType' => 'inputDateTime',
                    'eval' => 'datetime',
                ],
            ],
            'event' => [
                'exclude' => true,
                'label' => "$ll:$table.event",
                'config' => [
                    'type' => 'select',
                    'foreign_table' => 'tx_ricoevents_domain_model_event',
                ],
            ],
            'description' => [
                'exclude' => true,
                'label' => "$ll:$table.description",
                'config' => [
                    'type' => 'text',
                    'cols' => 40,
                    'rows' => 6,
                ],
                'defaultExtras' => 'richtext[]',
            ],
            'start_date_time' => [
                'exclude' => true,
                'label' => "$ll:$table.start_date_time",
                'config' => [
                    'type' => 'input',
                    'renderType' => 'inputDateTime',
                    'eval' => 'datetime,required',
                ],
            ],
            'end_date_time' => [
                'exclude' => true,
                'label' => "$ll:$table.end_date_time",
                'config' => [
                    'type' => 'input',
                    'renderType' => 'inputDateTime',
                    'eval' => 'datetime',
                ],
            ],
        ],
    ];
})(
    \Riconet\RicoEvents\Constants::EXTENSION_KEY,
    'tx_ricoevents_domain_model_day'
);
