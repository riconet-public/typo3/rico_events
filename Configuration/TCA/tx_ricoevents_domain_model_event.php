<?php

/**
 * This file is part of the "rico_events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

return (function ($extensionKey, $table) {
    /** @var \TYPO3\CMS\Core\Configuration\ExtensionConfiguration $extensionConfiguration */
    $extensionConfiguration = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
        \TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class
    );
    $pluginConfiguration = Riconet\RicoEvents\Utility\ConfigurationUtility::getPluginConfiguration('tx_ricoevents');
    if (isset($pluginConfiguration['view.']['flexformPath']) && !empty($pluginConfiguration['view.']['flexformPath'])) {
        $flexFormPath = 'FILE:' . $pluginConfiguration['view.']['flexformPath'] . 'Event.xml';
    } else {
        $flexFormPath = 'FILE:EXT:rico_events/Configuration/FlexForms/Domain/Event.xml';
    }
    $ll = "LLL:EXT:$extensionKey/Resources/Private/Language/locallang_db.xlf";
    $fields = implode(',', [
        'title',
        'path_segment',
        'teaser',
        'description',
        'location',
        'external_website',
        'image',
        'organizer',
        'start_date_time',
        'end_date_time',
        'archive_date_time',
        'days',
        'user_groups',
        'zip',
        'city',
        'street',
        'house_number',
        'categories',
    ]);

    return [
        'ctrl' => [
            'title' => "$ll:$table",
            'label' => 'title',
            'tstamp' => 'tstamp',
            'crdate' => 'crdate',
            'cruser_id' => 'cruser_id',
            'languageField' => 'sys_language_uid',
            'transOrigPointerField' => 'l10n_parent',
            'transOrigDiffSourceField' => 'l10n_diffsource',
            'delete' => 'deleted',
            'enablecolumns' => [
                'disabled' => 'hidden',
                'starttime' => 'starttime',
                'endtime' => 'endtime',
            ],
            'searchFields' => $fields,
            'iconfile' => "EXT:$extensionKey/Resources/Public/Icons/Event.svg",
            'default_sortby' => 'start_date_time',
        ],
        'types' => [
            '1' => [
                'showitem' => "--palette--;;paletteCore, --palette--;;paletteLanguage, $fields",
            ],
        ],
        'palettes' => [
            'paletteCore' => [
                'showitem' => 'hidden,',
            ],
            'paletteLanguage' => [
                'showitem' => 'sys_language_uid;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:sys_language_uid_formlabel,l10n_parent, l10n_diffsource,',
            ],
        ],
        'columns' => [
            'sys_language_uid' => [
                'exclude' => true,
                'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
                'config' => [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'special' => 'languages',
                    'foreign_table' => 'sys_language',
                    'foreign_table_where' => 'ORDER BY sys_language.title',
                    'items' => [
                        ['LLL:EXT:core/Resources/Private/Language/locallang_general.php:LGL.allLanguages', -1],
                    ],
                    'default' => 0,
                ],
            ],
            'l10n_parent' => [
                'displayCond' => 'FIELD:sys_language_uid:>:0',
                'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
                'config' => [
                    'type' => 'group',
                    'internal_type' => 'db',
                    'allowed' => $table,
                    'size' => 1,
                    'maxitems' => 1,
                    'minitems' => 0,
                    'default' => 0,
                ],
            ],
            'l10n_diffsource' => [
                'config' => [
                    'type' => 'passthrough',
                ],
            ],
            't3ver_label' => [
                'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel',
                'config' => [
                    'type' => 'input',
                    'size' => 30,
                    'max' => 255,
                ],
            ],
            'hidden' => [
                'exclude' => true,
                'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
                'config' => [
                    'type' => 'check',
                ],
            ],
            'starttime' => [
                'exclude' => true,
                'l10n_mode' => 'mergeIfNotBlank',
                'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
                'config' => [
                    'type' => 'input',
                    'renderType' => 'inputDateTime',
                    'eval' => 'datetime',
                ],
            ],
            'endtime' => [
                'exclude' => true,
                'l10n_mode' => 'mergeIfNotBlank',
                'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
                'config' => [
                    'type' => 'input',
                    'renderType' => 'inputDateTime',
                    'eval' => 'datetime',
                ],
            ],
            'title' => [
                'exclude' => false,
                'label' => "$ll:$table.title",
                'config' => [
                    'type' => 'input',
                    'size' => 30,
                    'eval' => 'trim,required',
                ],
            ],
            'path_segment' => [
                'exclude' => true,
                'label' => "$ll:$table.path_segment",
                'config' => [
                    'type' => 'slug',
                    'generatorOptions' => [
                        'fields' => ['title'],
                        'replacements' => [
                            '/' => '-',
                        ],
                    ],
                    'fallbackCharacter' => '-',
                    'eval' => (string) $extensionConfiguration->get($extensionKey, 'slugBehaviour'),
                    'default' => '',
                ],
            ],
            'teaser' => [
                'exclude' => false,
                'label' => "$ll:$table.teaser",
                'config' => [
                    'type' => 'text',
                    'enableRichtext' => true,
                    'cols' => 40,
                    'rows' => 3,
                ],
            ],
            'description' => [
                'exclude' => false,
                'label' => "$ll:$table.description",
                'config' => [
                    'type' => 'text',
                    'enableRichtext' => true,
                    'cols' => 40,
                    'rows' => 6,
                ],
            ],
            'location' => [
                'exclude' => false,
                'label' => "$ll:$table.location",
                'config' => [
                    'type' => 'input',
                    'size' => 30,
                    'eval' => 'trim',
                ],
            ],
            'zip' => [
                'exclude' => true,
                'label' => "$ll:$table.zip",
                'config' => [
                    'type' => 'input',
                    'size' => 30,
                    'eval' => 'trim',
                ],
            ],
            'city' => [
                'exclude' => true,
                'label' => "$ll:$table.city",
                'config' => [
                    'type' => 'input',
                    'size' => 30,
                    'eval' => 'trim',
                ],
            ],
            'street' => [
                'exclude' => true,
                'label' => "$ll:$table.street",
                'config' => [
                    'type' => 'input',
                    'size' => 30,
                    'eval' => 'trim',
                ],
            ],
            'house_number' => [
                'exclude' => true,
                'label' => "$ll:$table.house_number",
                'config' => [
                    'type' => 'input',
                    'size' => 30,
                    'eval' => 'trim',
                ],
            ],
            'country' => [
                'exclude' => true,
                'label' => "$ll:$table.country",
                'config' => [
                    'type' => 'input',
                    'size' => 30,
                    'eval' => 'trim',
                ],
            ],
            'image' => [
                'exclude' => false,
                'label' => "$ll:$table.image",
                'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                    'image',
                    [
                        'maxitems' => 1,
                        'appearance' => [
                            'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference',
                        ],
                        'overrideChildTca' => [
                            'types' => [
                                '0' => [
                                    'showitem' => '
                            --palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette',
                                ],
                                \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                                    'showitem' => '
                            --palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette',
                                ],
                            ],
                        ],
                    ],
                    $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
                ),
            ],
            'organizer' => [
                'exclude' => false,
                'label' => "$ll:$table.organizer",
                'config' => [
                    'type' => 'input',
                    'size' => 30,
                    'eval' => 'trim,required',
                ],
            ],
            'start_date_time' => [
                'exclude' => false,
                'label' => "$ll:$table.start_date_time",
                'config' => [
                    'type' => 'input',
                    'renderType' => 'inputDateTime',
                    'eval' => 'datetime,required',
                    'default' => 0,
                ],
            ],
            'end_date_time' => [
                'exclude' => false,
                'label' => "$ll:$table.end_date_time",
                'config' => [
                    'type' => 'input',
                    'renderType' => 'inputDateTime',
                    'eval' => 'datetime',
                    'default' => 0,
                ],
            ],
            'archive_date_time' => [
                'exclude' => false,
                'label' => "$ll:$table.archive_date_time",
                'config' => [
                    'type' => 'input',
                    'renderType' => 'inputDateTime',
                    'eval' => 'datetime',
                    'default' => 0,
                ],
            ],
            'flexform' => [
                'exclude' => true,
                'label' => "$ll:$table.flexform",
                'config' => [
                    'type' => 'flex',
                    'ds' => [
                        'default' => $flexFormPath,
                    ],
                    'default' => '',
                ],
            ],
            'days' => [
                'label' => "$ll:$table.days",
                'config' => [
                    'type' => 'inline',
                    'foreign_table' => 'tx_ricoevents_domain_model_day',
                    'maxitems' => 99,
                    'foreign_field' => 'event',
                ],
            ],
            'user_groups' => [
                'label' => "$ll:$table.user_groups",
                'config' => [
                    'type' => 'group',
                    'internal_type' => 'db',
                    'allowed' => 'fe_groups',
                    'foreign_table' => 'fe_groups',
                    'foreign_table_where' => '1=1 ORDER BY tx_ricoevents_domain_model_event_fe_groups_mm.sorting ASC',
                    'MM' => 'tx_ricoevents_domain_model_event_fe_groups_mm',
                    'MM_opposite_field' => 'tx_ricoevents_domain_model_event',
                    'size' => 6,
                ],
            ],
            'archived' => [
                'label' => "$ll:$table.archived",
                'config' => [
                    'type' => 'check',
                ],
            ],
            'external_website' => [
                'label' => "$ll:$table.external_website",
                'config' => [
                    'type' => 'input',
                    'renderType' => 'inputLink',
                ],
            ],
            'categories' => [
                'exclude' => true,
                'l10n_mode' => 'mergeIfNotBlank',
                'label' => "$ll:$table.categories",
                'config' => [
                    'type' => 'select',
                    'renderType' => 'selectTree',
                    'treeConfig' => [
                        'parentField' => 'parent',
                        'appearance' => [
                            'showHeader' => true,
                            'expandAll' => true,
                            'maxLevels' => 99,
                        ],
                    ],
                    'MM' => 'sys_category_record_mm',
                    'MM_match_fields' => [
                        'fieldname' => 'categories',
                        'tablenames' => 'tx_ricoevents_domain_model_event',
                    ],
                    'MM_opposite_field' => 'items',
                    'foreign_table' => 'sys_category',
                    'foreign_table_where' => ' AND (sys_category.sys_language_uid = 0 OR sys_category.l10n_parent = 0) ORDER BY sys_category.sorting',
                    'size' => 10,
                    'minitems' => 0,
                    'maxitems' => 99,
                ],
            ],
        ],
    ];
})(
    \Riconet\RicoEvents\Constants::EXTENSION_KEY,
    'tx_ricoevents_domain_model_event'
);
