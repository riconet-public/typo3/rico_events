<?php

/**
 * This file is part of the "rico_events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

defined('TYPO3_MODE') || exit();

(function ($extensionKey) {
    // The local lang file path.
    $ll = "LLL:EXT:$extensionKey/Resources/Private/Language/locallang_db.xlf";

    // Register events plugin.
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        "Riconet.$extensionKey",
        'events',
        "$ll:tx_ricoevents_events"
    );

    // Register calendar plugin.
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        "Riconet.$extensionKey",
        'calendar',
        "$ll:tx_ricoevents_calendar"
    );

    // Register events flex form.
    \Riconet\RicoEvents\Utility\PluginUtility::registerFlexForm(
        $extensionKey,
        'Configuration/FlexForms/Events.xml',
        'events'
    );

    // Register calendar flex form.
    \Riconet\RicoEvents\Utility\PluginUtility::registerFlexForm(
        $extensionKey,
        'Configuration/FlexForms/Calendar.xml',
        'calendar'
    );
})(\Riconet\RicoEvents\Constants::EXTENSION_KEY);
