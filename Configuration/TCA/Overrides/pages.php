<?php

/**
 * This file is part of the "rico_events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

defined('TYPO3_MODE') || exit();

(function ($extensionKey) {
    // Register page TS files.
    $extPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($extensionKey);
    $files = \TYPO3\CMS\Core\Utility\GeneralUtility::getFilesInDir(
        $extPath . 'Configuration/PageTS/',
        'txt'
    );
    foreach ($files as $fileKey => $fileValue) {
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
            $extensionKey,
            'Configuration/PageTSconfig/' . $fileValue,
            $fileValue
        );
    }
})(\Riconet\RicoEvents\Constants::EXTENSION_KEY);
