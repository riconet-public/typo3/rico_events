if (typeof $ !== 'undefined') {
  $(document).ready(function () {
    var $containers = $('.calendar-event-container');
    $containers.each(function () {
      var $container = $(this);
      var $events = $container.find('.event');
      $events.each(function () {
        var $event = $(this);
        var link = $event.find('.event-link').attr('href');
        var title = $event.find('.event-link').html();
        $container.parent().append('<a href="' + link + '" title="'
          + title + '" class="calendar-marker"></a>');
      });
    });

    $('.datepicker').datepicker({
      format: 'dd.mm.yyyy'
    });
  });
}
