if (typeof $ !== 'undefined') {
  $(document).ready(function () {
    $('.datepicker').datepicker({
      language: $('html')[0].lang,
      zIndexOffset: '8500',
      format: 'dd.mm.yyyy'  // @TODO Outsource this configuration.
    });
  });
}
