<?php

/**
 * This file is part of the "rico_events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

$EM_CONF[\Riconet\RicoEvents\Constants::EXTENSION_KEY] = [
    'title' => 'Events',
    'description' => 'This extension provides the possibility to manage event data.',
    'version' => '5.1.6',
    'category' => 'plugin',
    'constraints' => [
        'depends' => [
            'typo3' => '10.4.0-10.4.99'
        ]
    ],
    'state' => 'stable',
    'uploadfolder' => false,
    'clearCacheOnLoad' => true,
    'author' => 'Wolf Utz',
    'author_email' => 'w.utz@psv-neo.de',
    'author_company' => 'PSVneo',
    'autoload' => [
        'psr-4' => [
            'Riconet\\RicoEvents\\' => 'Classes',
        ],
    ],
    'autoload-dev' => [
        'psr-4' => [
            'Riconet\\RicoEvents\\Tests\\' => 'Tests',
        ],
    ],
];
