# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [5.1.0]
### Change
- Fix TCA
- Add slugs

## [5.0.0]
### Change
- TYPO3 10.4 support.
- Remove Backend module.
- Remove update wizard.
- Remove dead code.

## [3.1.2]
### Change
- migrated from bower.js to yarn

## [3.1.0]
### Add
- Add 404 handling for hidden or deleted events.
### Change
- Rename package

## [3.0.0]
### Change
- Change DATETIME table cols to timestamps (INT).
  Make sure to execute the update wizard to avoid loss of data!
