<?php

/**
 * This file is part of the "rico_events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace Riconet\RicoEvents\Filter;

use DateTime;
use TYPO3\CMS\Extbase\Domain\Model\Category;

class FilterArguments
{
    /**
     * @var \DateTime|null
     */
    protected $startDateTime;

    /**
     * @var \DateTime|null
     */
    protected $endDateTime;

    /**
     * @var string|null
     */
    protected $searchText = '';

    /**
     * @var \TYPO3\CMS\Extbase\Domain\Model\Category|null
     */
    protected $category;

    public function getStartDateTime(): ?DateTime
    {
        return $this->startDateTime;
    }

    public function setStartDateTime(?DateTime $startDateTime): void
    {
        $this->startDateTime = $startDateTime;
    }

    public function getEndDateTime(): ?DateTime
    {
        return $this->endDateTime;
    }

    public function setEndDateTime(?DateTime $endDateTime): void
    {
        $this->endDateTime = $endDateTime;
    }

    public function getSearchText(): ?string
    {
        return $this->searchText;
    }

    public function setSearchText(?string $searchText): void
    {
        $this->searchText = $searchText;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): void
    {
        $this->category = $category;
    }

    public function toArray(): array
    {
        return [
            'searchText' => $this->searchText ?? '',
            'category' => $this->category instanceof Category ? $this->category->getUid() : '',
            'startDateTime' => $this->startDateTime instanceof DateTime ?
                $this->startDateTime->format('Y-m-d') : '', // @TODO Outsource this configuration.
            'endDateTime' => $this->endDateTime instanceof DateTime ?
                $this->endDateTime->format('Y-m-d') : '', // @TODO Outsource this configuration.
        ];
    }

    /**
     * Validates a date time string by removing a H:i:s part of the date.
     */
    public static function validateDateString(string &$dateString): bool
    {
        if (empty(trim($dateString))) {
            return false;
        }
        $date = explode(' ', $dateString)[0];
        $dateString = date('d.m.Y', (int) strtotime($date)); // @TODO Outsource this configuration.

        return true;
    }

    protected function setTimeToZeroAM(DateTime $dateTime): DateTime
    {
        $dateTime->setTime(0, 0, 0);

        return $dateTime;
    }

    protected function setTimeToAlmost12PM(DateTime $dateTime): DateTime
    {
        $dateTime->setTime(23, 59, 59);

        return $dateTime;
    }
}
