<?php

/**
 * This file is part of the "rico_events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace Riconet\RicoEvents\Command;

use Doctrine\DBAL\Driver\Statement;
use Exception;
use PDO;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class ArchiveCommand extends Command
{
    public const SUCCESS = 0;
    public const FAILURE = 1;
    private const TABLE = 'tx_ricoevents_domain_model_event';

    /**
     * @var ConnectionPool
     */
    protected $connectionPool;

    public function __construct(ConnectionPool $connectionPool)
    {
        parent::__construct(null);
        $this->connectionPool = $connectionPool;
    }

    protected function configure(): void
    {
        $this->setDescription('This command moves expired events to a given PID.');
        $this->addArgument('archivePID', InputArgument::REQUIRED, 'The page id to archive to.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $archivePID = intval($input->getArgument('archivePID'));
            $events = $this->fetchEventsToArchive();
            if (0 === count($events)) {
                $output->writeln('No events to archive.');

                return self::SUCCESS;
            }
            $this->archiveEvents($events, $archivePID, $output);
        } catch (Exception $e) {
            $output->writeln('Error: ' . $e->getMessage());

            return self::FAILURE;
        }

        return self::SUCCESS;
    }

    protected function fetchEventsToArchive(): array
    {
        /** @var DeletedRestriction $deletedRestriction */
        $deletedRestriction = GeneralUtility::makeInstance(DeletedRestriction::class);
        $queryBuilder = $this->connectionPool->getQueryBuilderForTable(self::TABLE);
        $queryBuilder
            ->getRestrictions()
            ->removeAll()
            ->add($deletedRestriction);
        $result = $queryBuilder
            ->select('uid')
            ->from(self::TABLE)
            ->where(
                $queryBuilder->expr()->eq('archived', ':archived'),
                $queryBuilder->expr()->lte('archive_date_time', ':now'),
                $queryBuilder->expr()->gt('archive_date_time', 0)
            )
            ->setParameter('archived', false, PDO::PARAM_BOOL)
            ->setParameter('now', time(), PDO::PARAM_INT)
            ->execute();

        return $result instanceof Statement ? $result->fetchAll() : [];
    }

    protected function archiveEvents(array $events, int $archivePID, OutputInterface $output): void
    {
        $progressBar = new ProgressBar($output, count($events));
        $progressBar->start();
        foreach ($events as $event) {
            $this->archiveEvent($event, $archivePID);
            $progressBar->advance();
        }
        $progressBar->finish();
        $output->writeln('');
    }

    protected function archiveEvent(array $event, int $archivePID): void
    {
        $queryBuilder = $this->connectionPool->getConnectionForTable(self::TABLE);
        $queryBuilder->update(
            self::TABLE,
            [
                'archived' => true,
                'pid' => $archivePID,
            ],
            [
                'uid' => (int) $event['uid'],
            ]
        );
    }
}
