<?php

/**
 * This file is part of the "rico_events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace Riconet\RicoEvents\ViewHelpers;

use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class ModuloViewHelper extends AbstractViewHelper
{
    public function initializeArguments()
    {
        $this->registerArgument('value', 'int', '', true);
        $this->registerArgument('moduloValue', 'int', '', true);
        $this->registerArgument('then', 'string', '', false, '');
        $this->registerArgument('else', 'string', '', false, '');
    }

    public static function renderStatic(
        array $arguments,
        \Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ) {
        $value = (int) $arguments['value'];
        $moduloValue = (int) $arguments['moduloValue'];
        $then = (string) $arguments['then'] ?? '';
        $else = (string) $arguments['else'] ?? '';

        return $value % $moduloValue ? $then : $else;
    }
}
