<?php

/**
 * This file is part of the "rico_events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace Riconet\RicoEvents\ViewHelpers;

use DateTime;
use Riconet\RicoEvents\Domain\Model\Event;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class EventCalendarCompareViewHelper extends AbstractViewHelper
{
    public function initializeArguments()
    {
        $this->registerArgument(
            'event',
            Event::class,
            'The event to compare.',
            true
        );
        $this->registerArgument(
            'day',
            DateTime::class,
            'The date to compare.',
            true
        );
    }

    public static function renderStatic(
        array $arguments,
        \Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ) {
        /** @var Event $event */
        $event = $arguments['event'];
        /** @var DateTime $day */
        $day = $arguments['day'];

        $start = $event->getStartDateTime();
        if (!$start instanceof \DateTime) {
            return false;
        }
        $start->setTime(0, 0, 0);
        $end = $event->getEndDateTime();
        if ($end instanceof \DateTime) {
            $end->setTime(0, 0, 0);
        } else {
            $end = $start;
        }

        return self::dateIsInBetween($start, $end, $day);
    }

    protected static function dateIsInBetween(DateTime $from, DateTime $to, DateTime $subject): bool
    {
        return $subject->getTimestamp() >= $from->getTimestamp() &&
                $subject->getTimestamp() <= $to->getTimestamp();
    }
}
