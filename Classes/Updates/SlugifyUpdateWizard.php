<?php

/**
 * This file is part of the "rico_events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace Riconet\RicoEvents\Updates;

use Riconet\RicoEvents\Domain\Manager\EventSlugManagerInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\Container\Container;
use TYPO3\CMS\Install\Updates\DatabaseUpdatedPrerequisite;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

class SlugifyUpdateWizard implements UpgradeWizardInterface
{
    /**
     * @var EventSlugManagerInterface
     */
    private $eventSlugManager;

    public function __construct()
    {
        /** @var Container $container */
        $container = GeneralUtility::makeInstance(Container::class);
        /** @var EventSlugManagerInterface $eventSlugManager */
        $eventSlugManager = $container->getInstance(EventSlugManagerInterface::class);
        $this->eventSlugManager = $eventSlugManager;
    }

    public function getIdentifier(): string
    {
        return self::class;
    }

    public function getTitle(): string
    {
        return '[EXT:rico_events] Updates "path_segment"s of event records';
    }

    public function getDescription(): string
    {
        return '[EXT:rico_events] Fills empty "path_segment"s of event records by title.';
    }

    public function executeUpdate(): bool
    {
        try {
            $this->eventSlugManager->fillEmptySlugs();

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function updateNecessary(): bool
    {
        return $this->eventSlugManager->getEmptySlugCount() > 0;
    }

    public function getPrerequisites(): array
    {
        return [
            DatabaseUpdatedPrerequisite::class,
        ];
    }
}
