<?php

/**
 * This file is part of the "rico_events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace Riconet\RicoEvents;

class Constants
{
    public const EXTENSION_KEY = 'rico_events';

    public const EXTENSION_NAME = 'RicoEvents';

    public const PLUGIN_NAME_EVENTS = 'events';

    public const PLUGIN_NAME_CALENDAR = 'calendar';
}
