<?php

/**
 * This file is part of the "rico_events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace Riconet\RicoEvents\Domain\Model;

use TYPO3\CMS\Core\Service\FlexFormService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Annotation\ORM\Cascade;
use TYPO3\CMS\Extbase\Annotation\ORM\Lazy;
use TYPO3\CMS\Extbase\Annotation\Validate;
use TYPO3\CMS\Extbase\Domain\Model\Category as T3Category;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\Domain\Model\FrontendUserGroup;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

class Event extends Location
{
    /**
     * @var string|null
     *
     * @Validate("NotEmpty")
     */
    protected $title;

    /**
     * @var string|null
     *
     * @Validate("NotEmpty")
     */
    protected $pathSegment;

    /**
     * @var string|null
     */
    protected $description;

    /**
     * @var string|null
     */
    protected $teaser;

    /**
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference|null
     */
    protected $image;

    /**
     * @var string|null
     */
    protected $organizer;

    /**
     * @var \DateTime|null
     *
     * @Validate("NotEmpty")
     */
    protected $startDateTime;

    /**
     * @var \DateTime|null
     */
    protected $endDateTime;

    /**
     * @var \DateTime|null
     */
    protected $archiveDateTime;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\Category>|null
     *
     * @Lazy()
     */
    protected $categories;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Riconet\RicoEvents\Domain\Model\Day>|null
     *
     * @Lazy()
     *
     * @Cascade("remove")
     */
    protected $days;

    /**
     * @var string|null
     */
    protected $flexform;

    /**
     * @var bool|null
     */
    protected $archived;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FrontendUserGroup>|null
     *
     * @Lazy()
     */
    protected $userGroups;

    /**
     * @var string|null
     */
    protected $externalWebsite;

    /**
     * @var FlexFormService
     */
    protected $flexFormService;

    public function __construct()
    {
        $this->initStorageObjects();
        $this->initFlexformService();
    }

    public function getFlexform(): array
    {
        if (!is_string($this->flexform) || empty($this->flexform)) {
            return [];
        }

        return $this->flexFormService->convertFlexFormContentToArray($this->flexform);
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    public function getPathSegment(): ?string
    {
        return $this->pathSegment;
    }

    public function setPathSegment(?string $pathSegment): void
    {
        $this->pathSegment = $pathSegment;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getTeaser(): ?string
    {
        return $this->teaser;
    }

    public function setTeaser(?string $teaser): void
    {
        $this->teaser = $teaser;
    }

    public function getImage(): ?FileReference
    {
        return $this->image;
    }

    public function setImage(?FileReference $image): void
    {
        $this->image = $image;
    }

    public function getOrganizer(): ?string
    {
        return $this->organizer;
    }

    public function setOrganizer(?string $organizer): void
    {
        $this->organizer = $organizer;
    }

    public function getStartDateTime(): ?\DateTime
    {
        return $this->startDateTime;
    }

    public function setStartDateTime(?\DateTime $startDateTime): void
    {
        $this->startDateTime = $startDateTime;
    }

    public function getEndDateTime(): ?\DateTime
    {
        return $this->endDateTime;
    }

    public function setEndDateTime(?\DateTime $endDateTime): void
    {
        $this->endDateTime = $endDateTime;
    }

    public function getArchiveDateTime(): ?\DateTime
    {
        return $this->archiveDateTime;
    }

    public function setArchiveDateTime(?\DateTime $archiveDateTime): void
    {
        $this->archiveDateTime = $archiveDateTime;
    }

    public function getCategories(): ?ObjectStorage
    {
        return $this->categories;
    }

    public function setCategories(?ObjectStorage $categories): void
    {
        $this->categories = $categories;
    }

    public function getDays(): ?ObjectStorage
    {
        return $this->days;
    }

    public function setDays(?ObjectStorage $days): void
    {
        $this->days = $days;
    }

    public function getArchived(): ?bool
    {
        return $this->archived;
    }

    public function setArchived(?bool $archived): void
    {
        $this->archived = $archived;
    }

    public function getUserGroups(): ?ObjectStorage
    {
        return $this->userGroups;
    }

    public function setUserGroups(?ObjectStorage $userGroups): void
    {
        $this->userGroups = $userGroups;
    }

    public function getExternalWebsite(): ?string
    {
        return $this->externalWebsite;
    }

    public function setExternalWebsite(?string $externalWebsite): void
    {
        $this->externalWebsite = $externalWebsite;
    }

    public function addCategory(T3Category $category): void
    {
        if ($this->categories instanceof ObjectStorage) {
            $this->categories->attach($category);
        }
    }

    public function removeCategory(T3Category $category): void
    {
        if ($this->categories instanceof ObjectStorage) {
            $this->categories->detach($category);
        }
    }

    public function removeAllCategories(): void
    {
        if ($this->categories instanceof ObjectStorage) {
            $this->categories->removeAll($this->categories);
        }
    }

    public function addUserGroup(FrontendUserGroup $userGroup): void
    {
        if ($this->userGroups instanceof ObjectStorage) {
            $this->userGroups->attach($userGroup);
        }
    }

    public function removeUserGroup(FrontendUserGroup $userGroup): void
    {
        if ($this->userGroups instanceof ObjectStorage) {
            $this->userGroups->detach($userGroup);
        }
    }

    protected function initFlexformService(): void
    {
        /** @var FlexFormService $flexFormService */
        $flexFormService = GeneralUtility::makeInstance(FlexFormService::class);
        $this->flexFormService = $flexFormService;
    }

    protected function initStorageObjects(): void
    {
        $this->categories = new ObjectStorage();
        $this->days = new ObjectStorage();
        $this->userGroups = new ObjectStorage();
    }
}
