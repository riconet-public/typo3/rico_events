<?php

/**
 * This file is part of the "rico_events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace Riconet\RicoEvents\Domain\Repository;

use Riconet\RicoEvents\Filter\FilterArguments;
use TYPO3\CMS\Extbase\Domain\Model\Category;
use TYPO3\CMS\Extbase\Persistence\Generic\Qom\ConstraintInterface;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;

class EventRepository extends Repository
{
    protected $defaultOrderings = [
        'start_date_time' => QueryInterface::ORDER_ASCENDING,
        'title' => QueryInterface::ORDER_ASCENDING,
    ];

    public function findByCategories(array $categories, bool $showArchivedEvents = false): array
    {
        $query = $this->createQuery();
        $query->matching(
            $query->logicalAnd([
                $query->in('categories.uid', $categories),
                $query->equals('archived', $showArchivedEvents),
            ])
        );
        $result = $query->execute();

        return $result->toArray();
    }

    /**
     * @param ?int $limit
     *
     * @return QueryResultInterface
     */
    public function findFiltered(
        FilterArguments $filterArguments = null,
        array $categories = [],
        bool $showArchivedEvents = false,
        array $sorting = ['uid', 'ASC'],
        ?int $limit = null
    ) {
        $query = $this->createQuery();
        // Set limit if set.
        if ((int) $limit > 0) {
            $query->setLimit((int) $limit);
        }
        // Set ordering.
        $ordering = 'DESC' === $sorting[1] ? QueryInterface::ORDER_DESCENDING : QueryInterface::ORDER_ASCENDING;
        $query->setOrderings([
            (string) $sorting[0] => $ordering,
        ]);
        /** @var ConstraintInterface[] $constraints */
        $constraints = [];
        // Categories
        if (count($categories) > 0) {
            /** @var Category $category */
            foreach ($categories as $category) {
                $constraints[] = $query->contains('categories', $category);
            }
        }
        // FilterArguments
        if ($filterArguments instanceof FilterArguments) {
            $constraints = array_merge(
                $constraints,
                $this->getFilterArgumentsConstraints($query, $filterArguments)
            );
        }

        // Archived
        if ($showArchivedEvents) {
            $constraints[] = $query->equals('archived', true);
        }
        // Return findAll if no constraints are set.
        if (count($constraints) < 1) {
            /** @var QueryResultInterface $result */
            $result = $this->findAll($showArchivedEvents, $sorting, $limit);

            return $result;
        }
        /** @var QueryResultInterface $result */
        $result = $query->matching(
            $query->logicalAnd(
                $query->logicalAnd($constraints)
            )
        )->execute();

        return $result;
    }

    public function findAllAndDontRespectPID(): array
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        $result = $query->execute();

        return $result->toArray();
    }

    public function findAll(bool $showArchivedEvents = false, array $sorting = ['uid', 'ASC'], ?int $limit = null)
    {
        $query = $this->createQuery();
        // Set limit if set.
        if ((int) $limit > 0) {
            $query->setLimit((int) $limit);
        }
        $query->matching(
            $query->equals('archived', $showArchivedEvents)
        );
        $ordering = 'DESC' === $sorting[1] ? QueryInterface::ORDER_DESCENDING : QueryInterface::ORDER_ASCENDING;
        $query->setOrderings([
            (string) $sorting[0] => $ordering,
        ]);
        $result = $query->execute();

        return $result->toArray();
    }

    /**
     * @return ConstraintInterface[]
     */
    protected function getFilterArgumentsConstraints(QueryInterface $query, FilterArguments $filterArguments): array
    {
        $constraints = [];
        // Filter by category.
        if ($filterArguments->getCategory() instanceof Category) {
            $constraints[] = $query->contains('categories', $filterArguments->getCategory());
        }
        // Filter title by searchText.
        if (!empty($filterArguments->getSearchText())) {
            $constraints[] = $query->like('title', '%' . $filterArguments->getSearchText() . '%');
        }
        // Filter by start date.
        if ($filterArguments->getStartDateTime() instanceof \DateTime) {
            $constraints[] = $query->greaterThanOrEqual(
                'start_date_time',
                $filterArguments->getStartDateTime()->getTimestamp()
            );
        }
        // Filter by end date.
        if ($filterArguments->getEndDateTime() instanceof \DateTime) {
            $constraints[] = $query->logicalOr([
                $query->logicalAnd([
                    $query->logicalNot(
                        $query->equals('end_date_time', 0)
                    ),
                    $query->lessThanOrEqual(
                        'start_date_time',
                        $filterArguments->getEndDateTime()->getTimestamp()
                    ),
                ]),
                $query->logicalAnd([
                    $query->equals('end_date_time', 0),
                    $query->lessThanOrEqual(
                        'start_date_time',
                        $filterArguments->getEndDateTime()->getTimestamp()
                    ),
                ]),
            ]);
        }

        return $constraints;
    }
}
