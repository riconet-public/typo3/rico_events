<?php

/**
 * This file is part of the "rico_events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace Riconet\RicoEvents\Utility;

use TYPO3\CMS\Backend\Utility\BackendUtility;

class TcaEventsUtility
{
    public function formatDayLabel(array &$parameters): void
    {
        $record = $this->fetchRecord($parameters['table'] ?? '', (int) ($parameters['row']['uid'] ?? 0));
        if (is_null($record)) {
            $parameters['title'] = 'No title';

            return;
        }
        $parameters['title'] = $this->createSpeakingLabel($record);
    }

    protected function fetchRecord(string $table, int $uid): ?array
    {
        return BackendUtility::getRecord($table, $uid);
    }

    protected function createSpeakingLabel(array $record): string
    {
        $startDate = date('d.m.Y H:i', ($record['start_date_time'] ?? 0));
        $endDate = date('d.m.Y H:i', ($record['end_date_time'] ?? 0));

        return $startDate . ' - ' . $endDate;
    }
}
