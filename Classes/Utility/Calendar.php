<?php

/**
 * This file is part of the "rico_events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace Riconet\RicoEvents\Utility;

use DateTime;

class Calendar
{
    /**
     * @var int
     */
    protected $month = 0;

    /**
     * @var int
     */
    protected $year = 0;

    /**
     * @var array
     */
    protected $days = [];

    public function __construct()
    {
        $this->month = (int) date('n');
        $this->year = (int) date('Y');
        $this->buildCalendar();
    }

    public function getYear(): int
    {
        return $this->year;
    }

    public function getMonth(): int
    {
        return $this->month;
    }

    public function getDays(): array
    {
        return $this->days;
    }

    public function modifyMonth(int $value): void
    {
        $newValue = $this->month + $value;
        if ($newValue > 12) {
            $this->handlePositiveMonthModification($newValue);
        } elseif ($newValue <= 0) {
            $this->handleNegativeMonthModification($newValue);
        } else {
            $this->month += $value;
        }
        $this->days = [];
        $this->buildCalendar();
    }

    public function modifyYear(int $value): void
    {
        $this->year += $value;
        $this->days = [];
        $this->buildCalendar();
    }

    protected function buildCalendar(): void
    {
        $daysInMonth = date('t', (int) strtotime($this->year . '-' . $this->month . '-' . $this->month));
        for ($i = 1; $i <= $daysInMonth; ++$i) {
            $day = [
                'dateTime' => new DateTime("$this->year-$this->month-$i"),
                'number' => $i,
            ];
            $this->days[] = $day;
        }
        $this->addPreviousMonthDays();
        $this->addNextMonthDays();
    }

    /**
     * Adds days of the previous month until
     * the first day of the first week is reached.
     */
    protected function addPreviousMonthDays(): void
    {
        /* @var $firstDay DateTime */
        $firstDay = clone $this->days[0]['dateTime'];
        $firstDayNumber = (int) date('w', $firstDay->getTimestamp());
        for ($i = 0; $i < $firstDayNumber - 1; ++$i) {
            $clone = clone $firstDay->modify('-1 Day');
            $day = [
                'dateTime' => $clone,
                'number' => (int) $clone->format('d'),
            ];
            array_unshift($this->days, $day);
        }
    }

    /**
     * Adds days of the next month until
     * the last day of the last week is reached.
     */
    protected function addNextMonthDays(): void
    {
        /* @var $lastDay DateTime */
        $lastDay = clone $this->days[count($this->days) - 1]['dateTime'];
        $lastDayNumber = (int) date('w', $lastDay->getTimestamp());
        $missingDaysCount = 7 - $lastDayNumber;
        for ($i = 0; $i < $missingDaysCount; ++$i) {
            $clone = clone $lastDay->modify('+1 Day');
            $day = [
                'dateTime' => $clone,
                'number' => (int) $clone->format('d'),
            ];
            array_push($this->days, $day);
        }
    }

    /**
     * Handles  month modification with positive numbers greater than 12.
     */
    protected function handlePositiveMonthModification(int $value): void
    {
        $years = 1;
        $years += (int) round(($value - 12) / 13);
        $months = $value - (12 * $years);
        $this->year += $years;
        $this->month = $months;
    }

    /**
     * Handles  month modification with negative numbers greater than 12.
     */
    protected function handleNegativeMonthModification(int $negativeValue): void
    {
        $value = $negativeValue * -1;
        $years = -1;
        $years -= (int) round(($value - 12) / 13);
        $months = ($value - (12 * ($years * -1))) * -1;
        $this->year += $years;
        $this->month = $months;
    }
}
