<?php

/**
 * This file is part of the "rico_events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace Riconet\RicoEvents\Utility;

use Exception;
use TYPO3\CMS\Core\TypoScript\TypoScriptService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManager;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Object\ObjectManager;

class TypoScriptConfigurationUtility
{
    public static function configurationIsLoaded(string $extKey, string $evidenceKey = 'view'): bool
    {
        $configuration = self::getConfiguration();
        if (is_array($configuration) && count($configuration) > 0 && isset($configuration[$evidenceKey])) {
            return true;
        }
        throw new Exception("The configuration for the extension *$extKey* is not present or corrupted. Did you include the static file template?", 1499109032);
    }

    /**
     * @param ?string $extensionName
     * @param ?string $pluginName
     *
     * @return mixed
     */
    public static function getConfigurationByKey(
        array $groupKeyPair,
        ?string $extensionName = null,
        ?string $pluginName = null
    ) {
        // Check if param is valid.
        if (!(isset($groupKeyPair[key($groupKeyPair)]))) {
            throw new Exception("The given array is in a wrong format! Use the following format: ['group' => 'key']", 1499107468);
        }
        $configuration = self::getConfiguration($extensionName, $pluginName);

        // Check if group key exist in TS configuration.
        if (!(isset($configuration[key($groupKeyPair)]))) {
            throw new Exception('Could not find group key: ' . key($groupKeyPair) . '! Did you include the static file template?', 1499106879);
        }
        $group = $configuration[key($groupKeyPair)];

        // Check if key exist in the given group.
        if (!(array_key_exists($groupKeyPair[key($groupKeyPair)], $group))) {
            throw new Exception('Could not find key: ' . $groupKeyPair[key($groupKeyPair)] . ' in group: ' . key($groupKeyPair) . '! Did you include the static file template?', 1499106983);
        }
        $key = $groupKeyPair[key($groupKeyPair)];

        return $group[$key];
    }

    public static function getConfiguration(?string $extensionName = null, ?string $pluginName = null): array
    {
        /** @var ObjectManager $objectManager */
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        /** @var ConfigurationManager $configurationManager */
        $configurationManager = $objectManager->get(ConfigurationManager::class);

        return $configurationManager->getConfiguration(
            ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK,
            $extensionName,
            $pluginName
        );
    }

    public static function getAllConfiguration(): array
    {
        /** @var ObjectManager $objectManager */
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        /** @var ConfigurationManager $configurationManager */
        $configurationManager = $objectManager->get(ConfigurationManager::class);
        /** @var TypoScriptService $typoScriptService */
        $typoScriptService = GeneralUtility::makeInstance(TypoScriptService::class);

        return $typoScriptService->convertTypoScriptArrayToPlainArray($configurationManager->getConfiguration(
            ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT
        ));
    }

    /**
     * @return mixed
     */
    public static function getPluginConfiguration(string $pluginTypoScriptName)
    {
        $allConfigurations = self::getAllConfiguration();

        // Check if group key exist in TS configuration.
        if (!isset($allConfigurations['plugin'][$pluginTypoScriptName])) {
            throw new Exception("Could not find plugin TypoScript with name:*$pluginTypoScriptName* Did you include the static file template?", 1499327056);
        }

        return $allConfigurations['plugin'][$pluginTypoScriptName];
    }
}
