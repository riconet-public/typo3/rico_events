<?php

/**
 * This file is part of the "rico_events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace Riconet\RicoEvents\Utility;

use TYPO3\CMS\Extbase\Mvc\Request;

class PaginateUtility
{
    public static function getArguments(Request $request): array
    {
        $arguments = (array) $request->getArguments();
        if ($request->hasArgument('@widget_0')) {
            $widget = (array) $request->getArgument('@widget_0');
            if (isset($widget['arguments'])) {
                $arguments = $widget['arguments'];
            }
        }

        return $arguments;
    }
}
