<?php

/**
 * This file is part of the "rico_events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace Riconet\RicoEvents\Helper;

use DateTime;

class ICalendarHelper
{
    /**
     * @var array
     */
    protected $events = [];

    /**
     * @var string
     */
    protected $name = '';

    /**
     * @var string
     */
    private $calendarTemplate = '';

    /**
     * @var string
     */
    private $eventTemplate = '';

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->setupCalendarTemplates($name);
        $this->setupEventTemplate();
    }

    public function render(bool $output = true, string $fileName = ''): string
    {
        $iCalendarContent = $this->calendarTemplate;
        //Add events.
        $iCalendarContent = str_replace(
            '###_EVENTS_###',
            implode('', $this->events),
            $iCalendarContent
        );
        if (false === $output) {
            return $iCalendarContent;
        }
        if (empty($fileName)) {
            $fileName = $this->name;
        }
        header('Content-type: text/calendar; charset=utf-8');
        header("Content-Disposition: inline; filename=$fileName.ics");
        echo $iCalendarContent;
        exit();
    }

    public function registerEvent(
        DateTime $start,
        DateTime $end,
        string $summary = '',
        string $description = '',
        string $location = '',
        string $url = ''
    ): void {
        $event = $this->eventTemplate;
        $event = str_replace('###_DTSTAMP_###', $this->getFormattedTimeString(), $event);
        $event = str_replace('###_UID_###', $this->getUid(), $event);
        $event = str_replace('###_DTSTART_###', $this->getFormattedTimeString($start->getTimestamp()), $event);
        $event = str_replace('###_DTEND_###', $this->getFormattedTimeString($end->getTimestamp()), $event);
        $event = str_replace('###_URL_###', $url, $event);
        $event = str_replace('###_DESCRIPTION_###', $description, $event);
        $event = str_replace('###_SUMMARY_###', $summary, $event);
        $event = str_replace('###_LOCATION_###', $location, $event);
        $this->events[] = $event;
    }

    protected function setupCalendarTemplates(string $name): void
    {
        $this->calendarTemplate = <<<CALENDAR
BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//riconet//iCalendar
X-WR-CALNAME:$name
CALSCALE:GREGORIAN
BEGIN:VTIMEZONE
TZID:Europe/Berlin
TZURL:http://tzurl.org/zoneinfo-outlook/Europe/Berlin
X-LIC-LOCATION:Europe/Berlin
BEGIN:DAYLIGHT
TZOFFSETFROM:+0100
TZOFFSETTO:+0200
TZNAME:CEST
DTSTART:19700329T020000
RRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=-1SU
END:DAYLIGHT
BEGIN:STANDARD
TZOFFSETFROM:+0200
TZOFFSETTO:+0100
TZNAME:CET
DTSTART:19701025T030000
RRULE:FREQ=YEARLY;BYMONTH=10;BYDAY=-1SU
END:STANDARD
END:VTIMEZONE
###_EVENTS_###
END:VCALENDAR
CALENDAR;
    }

    protected function setupEventTemplate(): void
    {
        $this->eventTemplate = <<<EVENT
BEGIN:VEVENT
DTSTAMP:###_DTSTAMP_###
UID:###_UID_###
DTSTART;TZID="Europe/Berlin":###_DTSTART_###
DTEND;TZID="Europe/Berlin":###_DTEND_###
SUMMARY:###_SUMMARY_###
URL:###_URL_###
DESCRIPTION:###_DESCRIPTION_###
LOCATION:###_LOCATION_###
END:VEVENT
EVENT;
    }

    private function getUid(): string
    {
        return md5(uniqid((string) mt_rand(), true)) . '@ICalendarHelper.php';
    }

    private function getFormattedTimeString(?int $timestamp = null): string
    {
        if (!is_null($timestamp)) {
            return gmdate('Ymd', $timestamp) . 'T' . gmdate('His', $timestamp);
        }

        return gmdate('Ymd') . 'T' . gmdate('His');
    }
}
