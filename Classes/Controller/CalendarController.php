<?php

/**
 * This file is part of the "rico_events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace Riconet\RicoEvents\Controller;

use Riconet\RicoEvents\Helper\CalendarHelper;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class CalendarController extends AbstractEventsController
{
    /**
     * @var int
     */
    protected $monthsBefore = 0;

    /**
     * @var int
     */
    protected $monthsAfter = 0;

    /**
     * @var array
     */
    protected $calendars = [];

    public function initializeAction(): void
    {
        parent::initializeAction();
        $this->monthsBefore = intval($this->settings['months']['before']);
        $this->monthsAfter = intval($this->settings['months']['after']);
    }

    public function indexAction(): void
    {
        $events = count($this->categories) > 0 ? $this->eventRepository->findByCategories(
            $this->categories,
            $this->showArchivedEvents
        ) : $this->eventRepository->findAll($this->showArchivedEvents);
        $this->calendars = $this->createCalendars($this->monthsBefore, $this->monthsAfter);
        $this->view->assignMultiple([
            'events' => $events,
            'calendars' => $this->calendars,
        ]);
    }

    protected function createCalendars(int $before, int $after): array
    {
        /** @var CalendarHelper[] $calendars */
        $calendars = [];

        // Add previous calendars.
        for ($b = $before; $b > 0; --$b) {
            /** @var CalendarHelper $calendar */
            $calendar = GeneralUtility::makeInstance(CalendarHelper::class);
            $calendar->modifyMonth(-$b);
            $calendars[] = $calendar;
        }

        // Add actual calendars.
        /** @var CalendarHelper $calendar */
        $calendar = GeneralUtility::makeInstance(CalendarHelper::class);
        $calendars[] = $calendar;

        // Add upcoming calendars.
        for ($a = 1; $a <= $after; ++$a) {
            /** @var CalendarHelper $calendar */
            $calendar = GeneralUtility::makeInstance(CalendarHelper::class);
            $calendar->modifyMonth(+$a);
            $calendars[] = $calendar;
        }

        return $calendars;
    }
}
