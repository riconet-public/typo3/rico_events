<?php

/**
 * This file is part of the "rico_events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace Riconet\RicoEvents\Controller;

use Riconet\RicoEvents\Domain\Repository\EventRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Domain\Model\Category;
use TYPO3\CMS\Extbase\Domain\Repository\CategoryRepository;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

abstract class AbstractEventsController extends ActionController
{
    /**
     * @var EventRepository
     */
    protected $eventRepository;

    /**
     * @var CategoryRepository
     */
    protected $categoryRepository;

    /**
     * @var array
     */
    protected $categories = [];

    /**
     * @var int
     */
    protected $limit = 0;

    /**
     * @var bool
     */
    protected $showArchivedEvents = false;

    public function injectEventRepository(EventRepository $eventRepository): void
    {
        $this->eventRepository = $eventRepository;
    }

    public function injectCategoryRepository(CategoryRepository $categoryRepository): void
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function initializeAction(): void
    {
        // Get limit from plugin settings.
        $this->limit = intval($this->settings['restrictions']['limit']);
        $this->showArchivedEvents = boolval($this->settings['restrictions']['showArchivedEvents']);
        // Get categories from plugin settings.
        $categoryUids = GeneralUtility::intExplode(',', $this->settings['restrictions']['categories'], true);
        if (0 === count($categoryUids)) {
            return;
        }
        foreach ($categoryUids as $uid) {
            $category = $this->categoryRepository->findByUid($uid);
            if ($category instanceof Category) {
                $this->categories[] = $category;
            }
        }
    }
}
