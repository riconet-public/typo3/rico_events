<?php

/**
 * This file is part of the "rico_events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace Riconet\RicoEvents\Controller;

use DateTime;
use Exception;
use Riconet\RicoEvents\Domain\Model\Event;
use Riconet\RicoEvents\Filter\FilterArguments;
use Riconet\RicoEvents\Helper\CategoryHelper;
use Riconet\RicoEvents\Helper\ICalendarHelper;
use Riconet\RicoEvents\Utility\PaginateUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Annotation\IgnoreValidation;
use TYPO3\CMS\Extbase\Mvc\RequestInterface;
use TYPO3\CMS\Extbase\Mvc\ResponseInterface;
use TYPO3\CMS\Extbase\Property\Exception\TargetNotFoundException;
use TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter;
use TYPO3\CMS\Fluid\View\AbstractTemplateView;

class EventsController extends AbstractEventsController
{
    /**
     * @var CategoryHelper
     */
    protected $categoryHelper;

    /**
     * @var ?int
     */
    protected $limit;

    /**
     * @var string
     */
    protected $listTemplatePath = '';

    /**
     * @var string
     */
    protected $showTemplatePath = '';

    public function injectCategoryHelper(CategoryHelper $categoryHelper): void
    {
        $this->categoryHelper = $categoryHelper;
    }

    public function initializeAction(): void
    {
        parent::initializeAction();
        $this->limit = isset($this->settings['restrictions']['limit']) && $this->settings['restrictions']['limit'] > 0
            ? intval($this->settings['restrictions']['limit']) : null;
    }

    public function initializeListAction(): void
    {
        if (file_exists(GeneralUtility::getFileAbsFileName($this->settings['listTemplateLayout']))) {
            $this->listTemplatePath = GeneralUtility::getFileAbsFileName($this->settings['listTemplateLayout']);
        }
        if ($this->request->hasArgument('@widget_0') &&
            is_array($this->request->getArgument('@widget_0')) &&
            isset($this->request->getArgument('@widget_0')['arguments']) &&
            !empty($this->request->getArgument('@widget_0')['arguments'])
        ) {
            /** @var array $arguments */
            $arguments = $this->request->getArgument('@widget_0')['arguments'];
            FilterArguments::validateDateString($arguments['startDateTime']);
            FilterArguments::validateDateString($arguments['endDateTime']);
            $this->request->setArgument('filterArguments', $arguments);
        }
        if ($this->request->hasArgument('filterArguments')) {
            $propertyMappingConfiguration =
                $this->arguments->getArgument('filterArguments')->getPropertyMappingConfiguration();
            $propertyMappingConfiguration->forProperty('startDateTime')->setTypeConverterOption(
                DateTimeConverter::class,
                DateTimeConverter::CONFIGURATION_DATE_FORMAT,
                'd.m.Y' // @TODO Outsource this configuration.
            );
            $propertyMappingConfiguration->forProperty('endDateTime')->setTypeConverterOption(
                DateTimeConverter::class,
                DateTimeConverter::CONFIGURATION_DATE_FORMAT,
                'd.m.Y' // @TODO Outsource this configuration.
            );
            $propertyMappingConfiguration->allowAllProperties();
        }
    }

    public function initializeShowAction(): void
    {
        if (file_exists(GeneralUtility::getFileAbsFileName($this->settings['showTemplateLayout']))) {
            $this->showTemplatePath = GeneralUtility::getFileAbsFileName($this->settings['showTemplateLayout']);
        }
    }

    /**
     * @IgnoreValidation("filterArguments")
     */
    public function listAction(FilterArguments $filterArguments = null): void
    {
        if ($this->view instanceof AbstractTemplateView && !empty($this->listTemplatePath)) {
            $this->view->setTemplatePathAndFilename($this->listTemplatePath);
        }
        if (is_null($filterArguments) && boolval($this->settings['filter']['prefilter'])) {
            $filterArguments = new FilterArguments();
            $filterArguments->setStartDateTime(new DateTime());
        }
        $events = $this->eventRepository->findFiltered(
            $filterArguments,
            $this->categories,
            $this->showArchivedEvents,
            [
                $this->settings['sortingField'],
                $this->settings['sorting'],
            ],
            $this->limit > 0 ? $this->limit : null
        );
        $this->view->assignMultiple([
            'events' => $events,
            'categories' => $this->categories,
            'filterCategories' => $this->categoryHelper->getCategoriesByUids(
                GeneralUtility::intExplode(',', $this->settings['filter']['categories'])
            ),
            'arguments' => PaginateUtility::getArguments($this->request),
            'limit' => $this->limit > 0 ? $this->limit : null,
            'filterArguments' => $filterArguments,
            'urlEncodebleFilterArguments' => $filterArguments instanceof FilterArguments ?
                $filterArguments->toArray() : null,
        ]);
    }

    public function showAction(Event $event): void
    {
        if ($this->view instanceof AbstractTemplateView && !empty($this->showTemplatePath)) {
            $this->view->setTemplatePathAndFilename($this->showTemplatePath);
        }
        $this->view->assign('event', $event);
    }

    public function iCalendarAction(Event $event): void
    {
        $iCalendarHelper = new ICalendarHelper((string) $event->getTitle());
        $iCalendarHelper->registerEvent(
            $event->getStartDateTime() ?? new DateTime(),
            ($event->getEndDateTime() instanceof DateTime ? $event->getEndDateTime() : $event->getStartDateTime())
            ?? new DateTime(),
            strip_tags((string) $event->getTeaser()),
            strip_tags((string) $event->getDescription()),
            (string) $event->getLocation(),
            (string) $event->getExternalWebsite()
        );
        $iCalendarHelper->render(true);
    }

    public function processRequest(RequestInterface $request, ResponseInterface $response): void
    {
        try {
            parent::processRequest($request, $response);
        } catch (Exception $exception) {
            $this->handleKnownExceptions($exception);
        }
    }

    private function handleKnownExceptions(Exception $exception): void
    {
        if ('showAction' === $this->actionMethodName && $exception instanceof TargetNotFoundException) {
            $GLOBALS['TSFE']->pageNotFoundAndExit('Entity not found.');

            return;
        }
        throw $exception;
    }
}
