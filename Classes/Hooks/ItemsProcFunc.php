<?php

/**
 * This file is part of the "rico_events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

namespace Riconet\RicoEvents\Hooks;

use Exception;
use Riconet\RicoEvents\Utility\TypoScriptConfigurationUtility;

class ItemsProcFunc
{
    /**
     * Generates the template select label and values by TS.
     */
    public function listTemplateLayoutSelect(array &$config): void
    {
        // Get plugin configurations.
        $pluginEventsConfiguration = TypoScriptConfigurationUtility::getPluginConfiguration('tx_ricoevents');
        if (!isset($pluginEventsConfiguration['view']['listLayouts'])) {
            throw new Exception('Could not find TypoScript configuration for *plugin.tx_ricoevents.view.listLayouts!*', 1499327735);
        }
        $layouts = $pluginEventsConfiguration['view']['listLayouts'];
        // Add items by configuration.
        /** @var array $layout */
        foreach ($layouts as $layout) {
            if (isset($layout['label'], $layout['path'])) {
                $config['items'][] = [$layout['label'], $layout['path']];
            }
        }
    }

    /**
     * Generates the template select label and values by TS.
     */
    public function showTemplateLayoutSelect(array &$config): void
    {
        // Get plugin configurations.
        $pluginEventsConfiguration = TypoScriptConfigurationUtility::getPluginConfiguration('tx_ricoevents');
        if (!isset($pluginEventsConfiguration['view']['showLayouts'])) {
            throw new Exception('Could not find TypoScript configuration for *plugin.tx_ricoevents.view.showLayouts!*', 1499327735);
        }
        $layouts = $pluginEventsConfiguration['view']['showLayouts'];
        // Add items by configuration.
        /** @var array $layout */
        foreach ($layouts as $layout) {
            if (isset($layout['label'], $layout['path'])) {
                $config['items'][] = [$layout['label'], $layout['path']];
            }
        }
    }
}
