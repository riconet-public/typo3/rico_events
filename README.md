# Events

This extension provides the possibility to manage and display event data.
This extension comes with tow plugins.
One to display a filterable list of event data and another to display a calendar view by event data.

## Dependencies

- TYPO3 `^10.4`

## Installation

- Activate the extension via the Extensions module.
- Run 'yarn install' in the root of the extension.
- Include the static typo script file.

## Commands

### Archive events

Use this command to archive expired events.

> Only events with property `archiveDateTime` set and `archived` = 0 will be checked.

````shell script
vendor/bin/typo3 rico_events:archive ###_ARCHIVE_PID_###
````
