<?php

/**
 * This file is part of the "rico_events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

defined('TYPO3_MODE') || die();

(function ($extensionKey) {
    // Allow table on standard pages.
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages(
        'tx_ricoevents_domain_model_event'
    );
})(\Riconet\RicoEvents\Constants::EXTENSION_KEY);
